<?php

namespace ReclamationBundle\Controller;

use ReclamationBundle\Entity\Evaluation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EvluationController extends Controller
{

    public function evaluerAction(Request $request,$id)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $produit= $this->getDoctrine()->getRepository('ReclamationBundle:ProduitRec')->find($id);
        $comments= $this->getDoctrine()->getRepository('ReclamationBundle:Commentaire')->findBy(
            [
                'produit'=>$id
            ]
        );

        $evals= $this->getDoctrine()->getRepository('ReclamationBundle:Evaluation')->findBy(
            [
                'produit'=>$id
            ]
        );

        $moyenne= $this->getDoctrine()->getRepository('ReclamationBundle:Evaluation')->avgEvaluation($produit->getId());
        $produit->setMoyenne(array_sum($moyenne));

        $countev = count($evals);

        $dejaevalue= $this->getDoctrine()->getRepository('ReclamationBundle:Evaluation')->findEvaluateurDql($user->getId(),$id);
       // var_dump($dejaevalue);

        if($dejaevalue!=null) {
            $produit->setDejaevalue(true);
        }
        else {
            $produit->setDejaevalue(false);
        }

       // var_dump($produit->getDejaevalue(true));

        $avg= $this->getDoctrine()->getRepository('ReclamationBundle:Evaluation')->avgEvaluation($id);

       // var_dump($avg);

        $count = count($comments);
       if($request->isMethod('POST')){
            $evaluation = new Evaluation();
           $evaluation->setNote($request->get('rating'));
            $evaluation->setUser($user);
           $evaluation->setProduit($produit);
            //$reclamation->set(1);

            //    var_dump($reclamation);

            $em = $this->getDoctrine()->getManager();
            $em->persist($evaluation);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'votre evaluation a été ajouté avec succées ...!');

            $url = $this->generateUrl('list_prod');

            return $this->redirect($url);
        }
        return $this->render('@Reclamation/Default/evaluer.html.twig',array(
            'produit'=>$produit,
            'comments'=>$comments,
            'count'=>$count,
            'user'=>$user,
            'countev'=>$countev

        ));
    }

}
