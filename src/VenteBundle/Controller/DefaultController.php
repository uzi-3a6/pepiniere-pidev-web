<?php

namespace VenteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use VenteBundle\Entity\Commande;
use VenteBundle\Entity\commandes;
use VenteBundle\Entity\panier;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $produits = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->findAll();

        $em = $this->getDoctrine()->getManager();

        $panier = $em->getRepository('VenteBundle:panier')->findBy([
            "user"=>$user
        ]);
       // $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find(2);


        $sum = 0 ;

        foreach ($panier as $p ) {
            $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find($p->getIdProduit());
            $sum = $sum + ( $p->getQuantite() * $produit->getPrix());
            }


           // var_dump($sum);


        $count = count($panier);


        //var_dump($produits);

        return $this->render('@Vente/Default/index.html.twig',array(
            'produits'=>$produits,
            'count'=>$count,
            'panier'=>$panier,
            'sum'=>$sum
        ));
    }



    public function afficherPanierAction()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $panier = $em->getRepository('VenteBundle:panier')->findBy([
            "user"=>$user
        ]);
        // $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find(2);


        $sum = 0 ;

        foreach ($panier as $p ) {
            $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find($p->getIdProduit());
            $sum = $sum + ( $p->getQuantite() * $produit->getPrix());
        }


        // var_dump($sum);


        $count = count($panier);

        return $this->render('@Vente/Default/cart.html.twig',array(
            'count'=>$count,
            'panier'=>$panier,
            'sum'=>$sum
        ));
    }

    public function ajouterAction($id)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find($id);


        $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find($id);

        $em = $this->getDoctrine()->getManager();
        $panier2 = $em->getRepository('VenteBundle:panier')->findOneBy([
            "user"=>$user,
            "idProduit"=>$produit
        ]);


            if($panier2!=null && $panier2->getQuantite()<$produit->getQuantiteStock()) {
                $panier2->setQuantite($panier2->getQuantite()+1);

                $em = $this->getDoctrine()->getManager();
                $em->persist($panier2);
                $em->flush();
            }
            elseif($panier2!=null && $panier2->getQuantite()==$produit->getQuantiteStock())
            {

            }
            else {


                $panier = new panier();
                $panier->setUser($user);
                $panier->setIdCommande(1);
                $panier->setQuantite(1);
                $panier->setIdProduit($produit);
                $em = $this->getDoctrine()->getManager();
                $em->persist($panier);
                $em->flush();


            }


        return $this->redirectToRoute('AfficherClient');

    }




    public function supprimerAction($id)
    {

        $panier = $this->getDoctrine()->getRepository('VenteBundle:panier')->find($id);

        $em =$this->getDoctrine()->getManager();
        $em->remove($panier);
        $em->flush();

        $url = $this->generateUrl('cart_vente');
        return $this->redirect($url);

    }

    public function moinAction($id)
    {

        $panier = $this->getDoctrine()->getRepository('VenteBundle:panier')->find($id);
        if($panier->getQuantite()!=1)
        {
            $panier->setQuantite($panier->getQuantite()-1);
            $em =$this->getDoctrine()->getManager();
            $em->persist($panier);
            $em->flush();

        }

        $url = $this->generateUrl('cart_vente');
        return $this->redirect($url);

    }

    public function plusAction($id)
    {

        $panier2 = $this->getDoctrine()->getRepository('VenteBundle:panier')->find($id);

        if($panier2!=null && $panier2->getQuantite()<$panier2->getIdProduit()->getQuantiteStock()) {
            $panier2->setQuantite($panier2->getQuantite()+1);

            $em = $this->getDoctrine()->getManager();
            $em->persist($panier2);
            $em->flush();
        }


        $url = $this->generateUrl('cart_vente');
        return $this->redirect($url);

    }
    public function validerCommandeAction($id)
    {

        $commande = $this->getDoctrine()->getRepository('VenteBundle:commandes')->find($id);
        $commande->setEtat("Valide");
        $em =$this->getDoctrine()->getManager();
        $em->persist($commande);
        $em->flush();

        $url = $this->generateUrl('afficher_commande');
        return $this->redirect($url);

    }

    public function invaliderCommandeAction($id)
    {

        $commande = $this->getDoctrine()->getRepository('VenteBundle:commandes')->find($id);
        $commande->setEtat("Non Valide");
        $em =$this->getDoctrine()->getManager();
        $em->persist($commande);
        $em->flush();

        $url = $this->generateUrl('afficher_commande');
        return $this->redirect($url);

    }
    public function afficherCommandeAction()
    {
        $commande = $this->getDoctrine()->getRepository('VenteBundle:commandes')->findAll();


        return $this->render('@Vente/Default/commande.html.twig',array(
            'commande'=>$commande
        ));

    }
    public function afficherCommandeFrontAction()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $panier = $em->getRepository('VenteBundle:panier')->findBy([
            "user"=>$user
        ]);
        // $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find(2);


        $sum = 0 ;

        foreach ($panier as $p ) {
            $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find($p->getIdProduit());
            $sum = $sum + ( $p->getQuantite() * $produit->getPrix());
        }


        // var_dump($sum);


        $count = count($panier);

















        $commande = $em->getRepository('VenteBundle:commandes')->findBy([
            "user"=>$user
        ]);

        return $this->render('@Vente/Default/commandesFront.html.twig',array(
            'commande'=>$commande,
            'count'=>$count,
            'panier'=>$panier,
            'sum'=>$sum
        ));

    }






    public function annulerCommandeAction($id)
    {
        $commande = $this->getDoctrine()->getRepository('VenteBundle:commandes')->find($id);
        $em =$this->getDoctrine()->getManager();
        $em->remove($commande);
        $em->flush();
        $url = $this->generateUrl('afficher_commande_front');
        return $this->redirect($url);

    }





    public function ajouterCommandeAction()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $panier = $em->getRepository('VenteBundle:panier')->findBy([
            "user"=>$user
        ]);

        foreach ($panier as $p)
        {
            $commande = new commandes();
            $commande->setUser($user);
            $commande->setEtat("En Cours");
            $commande->setQuantite($p->getQuantite());
            $commande->setIdProduit($p->getIdProduit());
            $em = $this->getDoctrine()->getManager();
            $em->persist($commande);
            $em->flush();
        }

        foreach($panier as $p)
        {
            $em =$this->getDoctrine()->getManager();
            $em->remove($p);
            $em->flush();
        }
        $url = $this->generateUrl('cart_vente');
        return $this->redirect($url);

    }




    public function detailCommandeAction($id,Request $request)
    {




        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $panier = $em->getRepository('VenteBundle:panier')->findBy([
            "user"=>$user
        ]);
        // $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find(2);


        $sum = 0 ;

        foreach ($panier as $p ) {
            $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find($p->getIdProduit());
            $sum = $sum + ( $p->getQuantite() * $produit->getPrix());
        }


        // var_dump($sum);


        $count = count($panier);


        $com = $this->getDoctrine()->getRepository('VenteBundle:commandes')->find($id);

        return $this->render('@Vente/Default/detailcommande.html.twig',array(
            'com'=>$com,
            'count'=>$count,
            'panier'=>$panier,
            'sum'=>$sum
        ));
    }








    public function payerAction(Request $request)
    {


        if($_GET!=null)
        {


            $tot=$_GET['x'];

            $tot=(int)$tot*100;


            \Stripe\Stripe::setApiKey("sk_test_eKGopfgFoIXI9pDGcvkuICxT007Hyi7RrF");

            \Stripe\Charge::create(array(
                "amount" =>  $tot,
                "currency" => "eur",
                "source" => "tok_visa", // obtained with Stripe.js
                "description" => "Charge for madison.robinson@example.com",
                "receipt_email"=>"yasmine.kassar@esprit.tn",
            ));

            return $this->redirectToRoute('cart_vente');


        }

        return $this->render('@Vente/Default/payement.html.twig');






    }
}
