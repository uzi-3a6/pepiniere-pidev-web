<?php

namespace ForumBundle\Controller;

use EvenementBundle\Repository\testRepository;
use ForumBundle\Entity\Reponserc;
use ForumBundle\Entity\Spamsujetrc;
use ForumBundle\Entity\Sujetrc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ForumAdminController extends Controller
{
    public function indexAction()
    {
        $em=$this->getDoctrine()->getRepository(Sujetrc::class);
        $sujets = $em->findAll();

        return $this->render('@Forum/ForumAdmin/index.html.twig', array(
           'sujets'=>$sujets
        ));
    }
    public function  deleteAction($id){
        $em=$this->getDoctrine()->getRepository(Sujetrc::class);
        $sujet = $em->find($id);
        $this->getDoctrine()->getManager()->remove($sujet);
        $this->getDoctrine()->getManager()->flush();
$this->redirectToRoute('back');
    }
    public function  detailsujetAction($id){
        $em=$this->getDoctrine()->getRepository(Reponserc::class);
        $em2=$this->getDoctrine()->getRepository(Sujetrc::class);
        $sujet=$em2->find($id);
        $reponses = $em->findBy(array('idSujet'=>$id));
        return $this->render('@Forum/ForumAdmin/detailsujetback.html.twig',array('sujet'=>$sujet,'reponses'=>$reponses));

    }

    public  function deleterepAction($id,$idsujet){
        $em=$this->getDoctrine()->getRepository(Reponserc::class);
        $reponse = $em->find($id);
        $this->getDoctrine()->getManager()->remove($reponse);
        $this->getDoctrine()->getManager()->flush();
        return  $this->redirectToRoute('detailback',array('id'=>$idsujet));

    }
  public function listsujetspamAction(){
        $em=$this->getDoctrine()->getRepository(Spamsujetrc::class);
        $sujetsapm=$em->findAll();
        $em2=$this->getDoctrine()->getRepository(Sujetrc::class);
        $sujets = array();
        foreach ($sujetsapm as $sujet ){
            $s=$em2->find($sujet->idsujet);

            $s->id=$sujet->id;
            array_push($sujets,$s);

        }
        return $this->render('@Forum/ForumAdmin/listsujetspam.html.twig',array('sujets'=>$sujets));

  }
  public function  deletespamAction($id){
      $em=$this->getDoctrine()->getRepository(Spamsujetrc::class);
      $sujet = $em->find($id);
      $this->getDoctrine()->getManager()->remove($sujet);
      $this->getDoctrine()->getManager()->flush();
      return $this->redirectToRoute('sujetsapm');
  }
  public function statAction(){

      $em=$this->getDoctrine()->getRepository(Sujetrc::class);
      $plant=sizeof($em->findBy(array("categorie"=>"Plante")));
     $animaux=sizeof($em->findBy(array("categorie"=>"Animaux")));
      $arbuste=sizeof($em->findBy(array("categorie"=>"Arbuste")));
      $outil=sizeof($em->findBy(array("categorie"=>"Outil")));

      //******************************************************************************************


    $sujets = $em->findAll();

     $stat = array();
     foreach ( $sujets as $s ){
         if(array_key_exists($s->datesujet->format('Y-m-d'),$stat)){
             $stat[$s->datesujet->format('Y-m-d')]=$stat[$s->datesujet->format('Y-m-d')]+1;
         }else
         {
             $stat[$s->datesujet->format('Y-m-d')]=1;

         }
     }



      return $this->render('@Forum/ForumAdmin/statistique.html.twig',array("a"=>$plant
      ,"b"=>$animaux
      ,"c"=>$arbuste
      ,"d"=>$outil
      ,"stat"=>$stat));


  }

}
