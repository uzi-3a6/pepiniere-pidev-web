<?php

namespace ForumBundle\Repository;

/**
 * testRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SujetrcRepository extends \Doctrine\ORM\EntityRepository


{
    public function findDQL($mot)
    {
        $q = $this->getEntityManager()
            ->createQuery("SELECT v FROM ForumBundle:Sujetrc v
                      WHERE v.titresujet LIKE '%".$mot."%'");
        return $q->getResult();
    }



}
