<?php

namespace ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sujetrc
 *
 * @ORM\Table(name="sujetrc")
 * @ORM\Entity
 */
/**
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\SujetrcRepository")
 */
  class Sujetrc
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idSujetRc", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
      public $idsujetrc;

      /**
       *  @ORM\ManyToOne(targetEntity= MyApp\UserBundle\Entity\User::class) //mapping
       */

      public $user;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string", length=250, nullable=true)
     */
      public $contenu;

    /**
   * @var string
   *
   * @ORM\Column(name="categorie", type="string", length=45, nullable=true)
   */
      public $categorie;
      /**
       * @var string
       *
       * @ORM\Column(name="titresujet", type="string", length=45, nullable=true)
       */
      public $titresujet;
      /**
       * @var string
       *
       * @ORM\Column(name="image", type="string", length=45, nullable=true)
       */
      public $image;


      /**
     * @var integer
     *
     * @ORM\Column(name="idCategorieRc", type="integer", nullable=true)
     */
    public $idcategorierc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateSujet", type="datetime", nullable=true)
     */
    public $datesujet;

    /**
     * @var integer
     *
     * @ORM\Column(name="priorite", type="integer", nullable=true)
     */
    public $priorite;

    /**
     * @var integer
     *
     * @ORM\Column(name="resolue", type="integer", nullable=true)
     */
    public $resolue;




}

