<?php
/**
 * Created by PhpStorm.
 * User: zorgati
 * Date: 12/04/2019
 * Time: 12:03
 */

namespace ServiceBundle\Command;

use Doctrine\ORM\OptimisticLockException;
use ServiceBundle\Entity\Abonnement;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\DateTime;


class updateSerCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('test:maCommande')
            ->setDescription('Ceci est une commande de test')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln('Test command');

        $doctrine = $this->getContainer()->get('doctrine');
        $output->writeln("contaimer success) ");
        $em = $doctrine->getEntityManager();
        $output->writeln("manager success) ");

        try {
            $datee = new \DateTime('now');
        } catch (\Exception $e) {
        }
        $datee->modify('-5 days');
        $output->writeln("datee success) ");
        $abo=$em->createQuery('
        Delete FROM ServiceBundle:Abonnement a
        WHERE a.dateFin < :datef ')
        ->setParameter('datef', $datee->format("Y-m-d") );
        $output->writeln("query success) ");
        $abo->execute();
        $output->writeln("execute success) ");
        $output->writeln("datee success) ");



        $output->writeln("Doctrine worked, it didn't crashed :) ");
    }

}