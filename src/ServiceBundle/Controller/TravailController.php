<?php

namespace ServiceBundle\Controller;

use MyApp\UserBundle\Entity\User;
use ServiceBundle\Entity\SpecialiteJardinier;
use ServiceBundle\Entity\TravailJardinage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TravailController extends Controller
{
    public function chercherAction()
    {
        $em= $this->getDoctrine()->getManager();

        $jar= $em->getRepository(TravailJardinage::class)->Jardinier();

        $specjars=$em->getRepository(SpecialiteJardinier::class)->findBy(['user'=>$jar]);


        return $this->render('@Service/Travail/index.html.twig', array(

        ));
    }

    public function messagerieAction(){


        return $this->render('');
    }

}
