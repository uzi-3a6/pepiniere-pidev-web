<?php

namespace ServiceBundle\Controller;

use ServiceBundle\Entity\Services;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;


class AdminServiceController extends Controller
{
    public function afficherAction()
    {
        $em=$this->getDoctrine()->getManager();
        $services = $em->getRepository(Services::class)->findAll();

        return $this->render('@Service/AdminService/afficher.html.twig', array(
            'services'=>$services
        ));
    }
    public function ajouterAction(Request $request)
    {

        $em= $this->getDoctrine()->getManager();
        $service=new Services();

        if($request->isMethod('post')){

            $service->setNom($request->get('nomser'));
            $service->setDescription($request->get('descser'));
            $service->setTarif($request->get('tarifser'));

            $type=implode($request->get('opt'),';');
            $service->setType($type);

            $em->persist($service);
            $em->flush();
            return $this->redirectToRoute('admin_service_afficher');
        }
        return $this->render('@Service/AdminService/ajouter.html.twig', array(

        ));
    }
    public function modifierAction(Request $request)
    {

        $em= $this->getDoctrine()->getManager();
        $service=$em->getRepository(Services::class)->find($request->get('id'));

        if($request->isMethod('post')){

            $service->setNom($request->get('nomser'));
            $service->setDescription($request->get('descser'));
            $service->setTarif($request->get('tarifser'));
            $type=implode($request->get('opt'),';');
            $service->setType($type);
            $em->flush();
            return $this->redirectToRoute('admin_service_afficher');
        }
        return $this->render('@Service/AdminService/modifier.html.twig', array(
            'service'=>$service
        ));
    }
    public function supprimerAction(Request $request)
    {

        $em= $this->getDoctrine()->getManager();
        $service=$em->getRepository(Services::class)->find($request->get('id'));
        $em->remove($service);
        $em->flush();
        return $this->redirectToRoute('admin_service_afficher');


    }

}
