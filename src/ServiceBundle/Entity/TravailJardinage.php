<?php

namespace ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TravailJardinage
 *
 * @ORM\Table(name="travail_jardinage",uniqueConstraints={
        @ORM\UniqueConstraint(name="user_date_unique", columns={"jardinier_id", "dateTravail"})
        })
 * @ORM\Entity(repositoryClass="ServiceBundle\Repository\TravailJardinageRepository")
 */
class TravailJardinage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity= MyApp\UserBundle\Entity\User::class)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity= MyApp\UserBundle\Entity\User::class)
     */
    protected $jardinier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateTravail", type="date")
     */
    private $dateTravail;

    /**
     * @var int
     *
     * @ORM\Column(name="nbHeursTravail", type="integer")
     */
    private $nbHeursTravail;

    /**
     * @var int
     *
     * @ORM\Column(name="etatPaiement", type="integer")
     */
    private $etatPaiement;

    /**
     * @var int
     *
     * @ORM\Column(name="noteTravail", type="integer")
     */
    private $noteTravail;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateTravail
     *
     * @param \DateTime $dateTravail
     *
     * @return TravailJardinage
     */
    public function setDateTravail($dateTravail)
    {
        $this->dateTravail = $dateTravail;

        return $this;
    }

    /**
     * Get dateTravail
     *
     * @return \DateTime
     */
    public function getDateTravail()
    {
        return $this->dateTravail;
    }

    /**
     * Set nbHeursTravail
     *
     * @param integer $nbHeursTravail
     *
     * @return TravailJardinage
     */
    public function setNbHeursTravail($nbHeursTravail)
    {
        $this->nbHeursTravail = $nbHeursTravail;

        return $this;
    }

    /**
     * Get nbHeursTravail
     *
     * @return int
     */
    public function getNbHeursTravail()
    {
        return $this->nbHeursTravail;
    }

    /**
     * Set etatPaiement
     *
     * @param integer $etatPaiement
     *
     * @return TravailJardinage
     */
    public function setEtatPaiement($etatPaiement)
    {
        $this->etatPaiement = $etatPaiement;

        return $this;
    }

    /**
     * Get etatPaiement
     *
     * @return int
     */
    public function getEtatPaiement()
    {
        return $this->etatPaiement;
    }

    /**
     * Set noteTravail
     *
     * @param integer $noteTravail
     *
     * @return TravailJardinage
     */
    public function setNoteTravail($noteTravail)
    {
        $this->noteTravail = $noteTravail;

        return $this;
    }

    /**
     * Get noteTravail
     *
     * @return int
     */
    public function getNoteTravail()
    {
        return $this->noteTravail;
    }
}

