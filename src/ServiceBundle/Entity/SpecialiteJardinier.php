<?php

namespace ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpecialiteJardinier
 *
 * @ORM\Table(name="specialite_jardinier",uniqueConstraints={
                @ORM\UniqueConstraint(name="user_spec_unique", columns={"user_id", "specialite_id"})
                })
 * @ORM\Entity(repositoryClass="ServiceBundle\Repository\SpecialiteJardinierRepository")
 */
class SpecialiteJardinier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="tarif", type="float")
     */
    private $tarif;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity= MyApp\UserBundle\Entity\User::class)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity=Specialite::class)
     */
    protected $specialite;

    /**
     * Set tarif
     *
     * @param float $tarif
     *
     * @return SpecialiteJardinier
     */
    public function setTarif($tarif)
    {
        $this->tarif = $tarif;

        return $this;
    }

    /**
     * Get tarif
     *
     * @return float
     */
    public function getTarif()
    {
        return $this->tarif;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * @param mixed $specialite
     */
    public function setSpecialite($specialite)
    {
        $this->specialite = $specialite;
    }


}

