<?php

namespace ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorieproduit
 *
 * @ORM\Table(name="categorieproduit", indexes={@ORM\Index(name="idCategorieProduitParent", columns={"idCategorieProduitParent"})})
 * @ORM\Entity(repositoryClass="ProduitBundle\Repository\CategorieproduitRepository")
 */
class Categorieproduit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idCategorieProduit", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcategorieproduit;

    /**
     * @var string
     *
     * @ORM\Column(name="nomCategorie", type="string", length=45, nullable=true)
     */
    private $nomcategorie;

    /**
     * @var \Categorieproduit
     *
     * @ORM\ManyToOne(targetEntity="Categorieproduit")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="idCategorieProduitParent", referencedColumnName="idCategorieProduit")
     * })
     */
    private $idcategorieproduitparent;

    /**
     * @return int
     */
    public function getIdcategorieproduit()
    {
        return $this->idcategorieproduit;
    }

    /**
     * @param int $idcategorieproduit
     */
    public function setIdcategorieproduit($idcategorieproduit)
    {
        $this->idcategorieproduit = $idcategorieproduit;
    }

    /**
     * @return string
     */
    public function getNomcategorie()
    {
        return $this->nomcategorie;
    }

    /**
     * @param string $nomcategorie
     */
    public function setNomcategorie($nomcategorie)
    {
        $this->nomcategorie = $nomcategorie;
    }

    /**
     * @return \Categorieproduit
     */
    public function getIdcategorieproduitparent()
    {
        return $this->idcategorieproduitparent;
    }

    /**
     * @param \Categorieproduit $idcategorieproduitparent
     */
    public function setIdcategorieproduitparent($idcategorieproduitparent)
    {
        $this->idcategorieproduitparent = $idcategorieproduitparent;
    }


}

