<?php

namespace ProduitBundle\Controller;

use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use ProduitBundle\Entity\Categorieproduit;
use ProduitBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use VenteBundle\Entity\panier;

/**
 * Produit controller.
 *
 */
class ProduitController extends Controller
{
    /**
     * Lists all Produit entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $produits = $em->getRepository('ProduitBundle:Produit')->findAll();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        dump(get_class($paginator));
        $result=  $paginator->paginate(
            $produits,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',5)
        );


        return $this->render('@Produit/Produit/index.html.twig', array(
            'produits' => $result,
        ));
    }


    public function AfficherClientAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $panier=null;
        $count=0;
        $sum=0;
        if ($user = $this->getUser()){
            $panier = $em->getRepository(panier::class)->findBy([
                "user"=>$user
            ]);
            foreach ($panier as $p ) {

                $sum = $sum + ( $p->getQuantite() * $p->getIdProduit()->getPrix());
            }
            $count = count($panier);
        }

        if($request->get('enstock')=='1'){
            $produits= $em->getRepository('ProduitBundle:Produit')->Searchenstock();
        }elseif ($request->get('horstock')=='1'){
            $produits= $em->getRepository('ProduitBundle:Produit')->Searchhorsstock();
        }



        elseif ($request->get('memecat')!= null) {
            $produits = $em->getRepository('ProduitBundle:Produit')->Searchmemecategorie($request->get('memecat'));
        }elseif ($request->get('saisonEte')=='1') {
            $produits = $em->getRepository('ProduitBundle:Produit')->SearchsaisonEte();
        }elseif ($request->get('saisonPrintemps')=='1') {
            $produits = $em->getRepository('ProduitBundle:Produit')->SearchsaisonPrintemps();
        }elseif ($request->get('saisonAutomne')=='1') {
            $produits = $em->getRepository('ProduitBundle:Produit')->SearchsaisonAutomne();
        }elseif ($request->get('saisonHiver')=='1') {
            $produits = $em->getRepository('ProduitBundle:Produit')->SearchsaisonHiver();
        }elseif ($request->get('TrieAsc')=='1') {
            $produits = $em->getRepository('ProduitBundle:Produit')->TrieAsc();
        }elseif ($request->get('TrieDesc')=='1') {
            $produits = $em->getRepository('ProduitBundle:Produit')->TrieDesc();
        }elseif ($request->get('TrieAZ')=='1') {
    $produits = $em->getRepository('ProduitBundle:Produit')->TrieAZ();
        }
        else{
            $produits = $em->getRepository('ProduitBundle:Produit')->findAll();
        }

        $categories= $em->getRepository('ProduitBundle:Categorieproduit')->Searchsouscategories();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        dump(get_class($paginator));
        $result=  $paginator->paginate(
            $produits,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',5)
        );

        return $this->render('@Produit/Produit/AfficherClient.html.twig', array(
            'produits' => $result,
            'categories' => $categories,
            'count'=>$count,
            'panier'=>$panier,
            'sum'=>$sum
        ));
    }

    /**
     * Creates a new Produit entity.
     *
     */
    public function newAction(Request $request)
    {

        $em= $this->getDoctrine()->getManager();
      //$produit=$em->getRepository(Produit::class);
        $produit= new Produit();


        $categories= $em->getRepository(Categorieproduit::class)->findby(['idcategorieproduitparent'=>null]);
        if ($request->isMethod('POST')) {

                $produit->setNom($request->get('Nom'));
                $produit->setDescription($request->get('Description'));
                $produit->setPrix($request->get('Prix'));
                $produit->setDateajout(new \DateTime('now'));
                $produit->setQuantitestock($request->get('Quantitestock'));
                $produit->setImage($request->get('Image'));
                $produit->setSaison($request->get('Saison'));
                $cat = $em->getRepository(Categorieproduit::class)->find($request->get('Idcategorie'));
                $produit->setIdcategorie($cat);
                $em->persist($produit);
                $em->flush();

            return $this->redirectToRoute('produit_index');
        }

        return $this->render('@Produit/produit/new.html.twig', array(
            'produit' => $produit,
            'categories'=>$categories
        ));
    }

    public function verifAjoutAction(Request $request){

        if($request->isXmlHttpRequest()){
            $em= $this->getDoctrine()->getManager();
            $produit= $em->getRepository(Produit::class)->Searchememeproduit($request->get('nomp'));

            if ($produit==null){
                return new Response("pas de produit similaire",404 );
            }else{
                $jsonData = array();
                $idx = 0;
                foreach($produit as $prod) {

                    $temp = array(
                        'idp' => $prod->getIdproduit(),
                        'nom' => $prod->getNom(),
                        'img' => $prod->getImage()
                    );
                    $jsonData[$idx++] = $temp;
                }
                return new JsonResponse($jsonData);
            }
        }
    }

    /**
     * Deletes a Produit entity.
     *
     */
    public function deleteAction()
    {
        $produit=$this->getDoctrine()->getRepository(Produit::class);
        $obj=$produit->find($_GET['idproduit']);
        $produit=$this->getDoctrine()->getManager();
        $produit->remove($obj);
        $produit->flush();
        return $this->redirectToRoute('produit_index');
    }



    public function updateAction(Request $request,$idprod)
    {

        $em= $this->getDoctrine()->getManager();
        $produit= $em->getRepository(Produit::class)->find($idprod);
        $categories= $em->getRepository(Categorieproduit::class)->findby(['idcategorieproduitparent'=>null]);


        if ($request->isMethod('POST')) {
            $produit->setNom($request->get('Nom'));
            $produit->setDescription($request->get('Description'));
            $produit->setPrix($request->get('Prix'));
            $produit->setQuantitestock($request->get('Quantitestock'));
            $produit->setImage($request->get('Image'));
            $produit->setSaison($request->get('Saison'));
            $cat= $em->getRepository(Categorieproduit::class)->find($request->get('Idcategorie'));
            $produit->setIdcategorie($cat);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('produit_index');
        }

        return $this->render('@Produit/produit/edit.html.twig', array(
            'produit' => $produit,
            'categories'=> $categories
        ));
    }


    public function messageAction()
    {
        return $this->render('@Produit/produit/show.html.twig',array());
    }


    public function loadCategorieAction(Request $request)
    {
        if ($request->isXmlHttpRequest()){
            $idcatp= $request->get('idpc');
            $em= $this->getDoctrine()->getManager();
            $categories= $em->getRepository(Categorieproduit::class)->findby(['idcategorieproduitparent'=>$idcatp]);
            $jsonData = array();
            $idx = 0;
            foreach($categories as $cat) {

                $temp = array(
                    'idc' => $cat->getIdcategorieproduit(),
                    'idcp' => $cat->getIdcategorieproduitparent(),
                    'nom' => $cat->getNomcategorie()
                );
                $jsonData[$idx++] = $temp;
            }
            //var_dump($categories);
           // $datas = json_encode($categories);
            //return $this->render('@Produit/produit/test.html.twig');
            return new JsonResponse($jsonData);
        }
        return new Response("Erreur");
    }


    public function PdfAction($idprod)
    {
        $em = $this->getDoctrine()->getManager();
        $produit= $em->getRepository(Produit::class)->find($idprod);
        $name = 'produit'.$produit->getIdproduit().'_'.md5(date('Y-m-d H:i:s:u'));
        $this->get('knp_snappy.pdf')->generateFromHtml(
            $this->renderView(
                '@Produit/produit/pdf.html.twig',
                array(
                    'produit'  => $produit
                )
            ),
            'PDF2/'.$name.'.pdf'
        );
        return $this->redirectToRoute('produit_index');
    }

    public function rechercheAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $nom = $request->get('nom');
        $prix= $request->get('prix');
        $quantitestock =$request->get('quantitestock');
        $description =$request->get('description');

        $result = $this->getDoctrine()->getManager()->getRepository(Produit::class)->advancedSearch($nom,$prix,$quantitestock,$description);
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        dump(get_class($paginator));
        $result=  $paginator->paginate(
            $result,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',5)
        );


        return $this->render('@Produit/Produit/index.html.twig', array(
            'produits' => $result,
        ));

    }
    public function recherchecAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $nom = $request->get('nom');
        $prix= $request->get('prix');
        $quantitestock =$request->get('quantitestock');
        $description =$request->get('description');
        $categories= $em->getRepository('ProduitBundle:Categorieproduit')->Searchsouscategories();
        $result = $this->getDoctrine()->getManager()->getRepository(Produit::class)->advancedSearch($nom,$prix,$quantitestock,$description);
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        dump(get_class($paginator));
        $result=  $paginator->paginate(
            $result,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',5)
        );


        return $this->render('@Produit/Produit/AfficherClient.html.twig', array(
            'produits' => $result,
            'categories'=> $categories,
        ));

    }

    public function StatAction()
    {   $pieChart = new PieChart();
        $em= $this->getDoctrine();
        $produits= $em->getRepository(Produit::class)->findAll();

        $produitsHorsStock = $em->getRepository(Produit::class)->countProduitHorsStock();
        $produitsStock=$em->getRepository(Produit::class)->countProduitStock();
        $produitstotal=$em->getRepository(Produit::class)->countProduits();


        $data=array();
        $stat=['Disponibilite','nbproduit'];

        array_push($data,$stat);

        $stat=array();
        array_push($stat,"Hors stock",($produitsHorsStock*100/$produitstotal));
        array_push($stat,"En stock",($produitsStock*100/$produitsStock));
        $stat=["Hors stock",$produitsHorsStock*100/$produitsStock];
        array_push($data,$stat);
        $stat=["En stock",$produitsStock*100/$produitsStock];
        array_push($data,$stat);


        $pieChart->getData()->setArrayToDataTable($data);
        $pieChart->getOptions()->setTitle(' Pourcentage des produits');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);
        return $this->render('@Produit/Produit/stat.html.twig', array('piechart' => $pieChart
            ));

    }


    public function AfficherdetailAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $produits = $em->getRepository('ProduitBundle:Produit')->findAll();
        $produit = $em->getRepository('ProduitBundle:Produit')->find($request->get('id'));
        return $this->render('@Produit/produit/singleproduit.html.twig',array(
         'p'  => $produit,
            'prod'  => $produits,



        ));
    }

}
