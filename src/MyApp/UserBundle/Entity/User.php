<?php
/**
 * Created by PhpStorm.
 * User: Zorgati
 * Date: 23/03/2019
 * Time: 10:52
 */

namespace MyApp\UserBundle\Entity;
use FOS\MessageBundle\Model\ParticipantInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */

class User extends BaseUser implements ParticipantInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="role",type="integer")
     */
    private $role;
    /**
     * @ORM\Column(name="nom",type="string",length=255, nullable=true)
     */
    private $nom =null;
    /**
     * @ORM\Column(name="prenom",type="string",length=255, nullable=true)
     */
    private $prenom =null;

    /**
     * @ORM\OneToOne(targetEntity=ServiceBundle\Entity\Adresse::class)
     */
    protected $adress;


    /**
     * @ORM\Column(name="telephone",type="string",length=200, nullable=true)
     */
    private $telephone =null;

    /**
     * @ORM\Column(name="description_jardinier",type="string",length=250, nullable=true)
     */
    private $descriptionJardinier =null;

    /**
     * @ORM\Column(name="jours_travails",type="string",length=250, nullable=true)
     */
    private $joursTravails =null;

    /**
     * @ORM\Column(name="description_rc",type="string",length=250, nullable=true)
     */
    private $descriptionRc =null;

    /**
     * @ORM\Column(name="image",type="string",length=200, nullable=true)
     */
    private $image =null;




    public function __construct()
    {
        parent::__construct();

        $this->roles=array('ROLE_CLIENT');
        $this->role=1;
    }




    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getUsernameCanonical()
    {
        return $this->usernameCanonical;
    }

    /**
     * @param string $usernameCanonical
     */
    public function setUsernameCanonical($usernameCanonical)
    {
        $this->usernameCanonical = $usernameCanonical;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param mixed $adress
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

    /**
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * @param string $emailCanonical
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = null;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }



    /**
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param string $confirmationToken
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return \DateTime
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }



    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }



    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getDescriptionJardinier()
    {
        return $this->descriptionJardinier;
    }

    /**
     * @param mixed $descriptionJardinier
     */
    public function setDescriptionJardinier($descriptionJardinier)
    {
        $this->descriptionJardinier = $descriptionJardinier;
    }

    /**
     * @return mixed
     */
    public function getJoursTravails()
    {
        return $this->joursTravails;
    }

    /**
     * @param mixed $joursTravails
     */
    public function setJoursTravails($joursTravails)
    {
        $this->joursTravails = $joursTravails;
    }

    /**
     * @return mixed
     */
    public function getDescriptionRc()
    {
        return $this->descriptionRc;
    }

    /**
     * @param mixed $descriptionRc
     */
    public function setDescriptionRc($descriptionRc)
    {
        $this->descriptionRc = $descriptionRc;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }





}