<?php

namespace EvenementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evaluationstand
 *
 * @ORM\Table(name="evaluationstand",uniqueConstraints={@ORM\UniqueConstraint(name="stand_user_unique", columns={"idstand","iduser"} )})
 * @ORM\Entity
 */
class Evaluationstand
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idEvaluationStand", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idevaluationstand;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Stand::class)
     * @ORM\JoinColumn(name="idstand", referencedColumnName="idStand")
     */
    private $idstand;

    /**
     *
     * @ORM\ManyToOne(targetEntity=MyApp\UserBundle\Entity\User::class)
     * @ORM\JoinColumn(name="iduser", referencedColumnName="id")
     */
    private $iduser;

    /**
     * @var float
     *
     * @ORM\Column(name="note", type="float", precision=10, scale=0, nullable=true)
     */
    private $note;

    /**
     * @return int
     */
    public function getIdevaluationstand()
    {
        return $this->idevaluationstand;
    }

    /**
     * @param int $idevaluationstand
     */
    public function setIdevaluationstand($idevaluationstand)
    {
        $this->idevaluationstand = $idevaluationstand;
    }

    /**
     * @return int
     */
    public function getIdstand()
    {
        return $this->idstand;
    }

    /**
     * @param int $idstand
     */
    public function setIdstand($idstand)
    {
        $this->idstand = $idstand;
    }

    /**
     * @return int
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * @param int $iduser
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;
    }

    /**
     * @return float
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param float $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }



}

