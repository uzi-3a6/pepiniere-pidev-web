<?php

namespace EvenementBundle\Controller;

use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use EvenementBundle\Entity\Evenement;
use EvenementBundle\Entity\Reservation;
use EvenementBundle\Entity\Stand;
use EvenementBundle\Form\ModeleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EvenementBackController extends Controller
{
   /* public function indexAction()
    {
        return $this->render('@Evenement/Front/formulaireBack.html.twig');
    }*/




    public function afficheEvenementGeneraleAction()
    {
        $em=$this->getDoctrine()->getManager();
        $modeles=$em->getRepository("EvenementBundle:Evenement")->findAll();

        return $this->render("@Eve  nement/Front/AfficherEvenementBack.html.twig",array('m'=>$modeles));
    }


    public function AjouteEvenementAction(Request $request)
    {

        $model=new Evenement();
        if($request ->isMethod('POST'))
        {


            $model->setNomevenement($request->get('nom'));
            $model->setAdresse($request->get('adresse'));
            $model->setDatedebut(new\DateTime($request->get('datedebut')));
            $model->setDatefin(new\DateTime($request->get('datefin')));
            $model->setEtat("Disponible");
            $model->setNbplace($request->get('nombreduplace'));
            $model->setNbstand($request->get('nombredustand'));
            $model->setPrix($request->get('prix'));

            $model->setImage($request->get('Image'));


            $file=$request->files->get('img');
            $filName=md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('images_directory'),
                $filName);

            $model->setImage($filName);


            $em=$this->getDoctrine()->getManager();
            $em->persist($model);
            $em->flush();

            return $this->redirectToRoute('afficherEvenemntBack');
        }


        return $this->render("@Evenement/Front/formulaireBack.html.twig",array());
    }



    public function SupprimerEvenementAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository("EvenementBundle:Evenement")->find($id);
        $em->remove($modele);
        $em->flush();
        return $this->redirectToRoute('afficherEvenemntBack');
    }


    public function  ModifierEvenementAction($id , Request $request )
    {



        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository(Evenement::class)->find($id);

        if($request->isMethod('POST')){

            $evenements->setNomevenement($request->get('nom'));
            $evenements->setAdresse($request->get('adresse'));
            $evenements->setDatedebut(new\DateTime($request->get('datedebut')));
            $evenements->setDatefin(new\DateTime($request->get('datefin')));
            if($evenements->getDatefin()< new \DateTime()){
                $evenements->setEtat("Terminer");

            }
            else{
                $evenements->setEtat("Disponible");


            }
            if($request->get('nombreduplace')==0){
                $evenements->setEtat("Complet");
            }
            else{
                $evenements->setEtat("Disponible");

            }

            $evenements->setNbplace($request->get('nombreduplace'));
            $evenements->setNbstand($request->get('nombredustand'));
            $evenements->setPrix($request->get('prix'));



                //$evenements->setImage($request->get('img'));

                $file=$request->files->get('img');


                $filName=md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('images_directory'),
                    $filName);

                $evenements->setImage($filName);



            $em = $this->getDoctrine()->getManager();
           /// $em->persist($evenements);
            $em->flush();
            return $this->redirectToRoute('afficherEvenemntBack');
        }

        return $this->render('@Evenement/Front/ModifierEvenementBack.html.twig', array(
            'evenements' =>$evenements));
    }

    public function statistiqueAction()
    {
        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            [['Task', 'Hours per Day'],
                ['note > à 7',     11],
                ['note entre 5 et 7',      2],
                ['note <5',  2]
            ]
        );
        $pieChart->getOptions()->setTitle('Stand par note');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);

        return $this->render('@Evenement/Front/stat.html.twig', array('piechart' => $pieChart));
    }
    public function reservationsAction($id){
        $event=$this->getDoctrine()->getRepository(Evenement::class)->find($id);



            $em=$this->getDoctrine()->getRepository(Reservation::class);
        $reservation=$em->findBy(array("event"=>$event));
        return   $this->render('@Evenement/Front/reservation.html.twig',array("reservation"=>$reservation));

    }
    public function standAction($id){
        $event=$this->getDoctrine()->getRepository(Evenement::class)->find($id);


        $em=$this->getDoctrine()->getRepository(Stand::class);
        $reservation=$em->findBy(array("idevenement"=>$event));
        return   $this->render('@Evenement/Front/stand.html.twig',array("reservation"=>$reservation));

    }



}
