<?php

namespace EvenementBundle\Controller;

use EvenementBundle\Entity\Evaluationstand;
use EvenementBundle\Entity\Evenement;
use EvenementBundle\Entity\Reservation;
use EvenementBundle\Entity\Stand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;



class EvenementController extends Controller
{
    public function indexAction(Request $request)
    {
        $evenement = new Evenement();
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository(Evenement::class)->findAll();



        return $this->render('@Evenement/Front/AfficherEvenement.html.twig', ['evenements' => $evenement]);

    }
    public function EventDeatailsAction($id,Request $request)
    {   $user=$this->getUser();
        $evenement = new Evenement();
        $today_startdatetime = new\DateTime('now');

        $em = $this->getDoctrine()->getManager();
        $em->getRepository(Evenement::class)->UpdateEtatEvenement("Complet",$id);
        $em->getRepository(Evenement::class)->UpdateEtatTEvenement("Terminer",$id,$today_startdatetime);
        $evenement = $em->getRepository(Evenement::class)->find($id);

        $stand = $em->getRepository(Stand::class)->findstand($id);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $stand, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            1 /*limit per page*/
        );

        if($request->isMethod('POST')){
            $standeva = $em->getRepository(Stand::class)->find($request->get('idstand'));
            $evaluationstand = $em->getRepository(Evaluationstand::class)->findOneby(['iduser'=>$user,'idstand'=>$standeva]);


            if ($evaluationstand === null){
                $evaluationstand=new Evaluationstand();
                $evaluationstand->setIduser($user);
                $evaluationstand->setIdstand($standeva);
                $evaluationstand->setNote($request->get('noterating'));
                $em->persist($evaluationstand);
                $em->flush();
            }else{
                $evaluationstand->setNote($request->get('noterating'));
                $em->flush();
            }
            return $this->redirectToRoute('afficherEvenemnt');
        }
        return $this->render('@Evenement/Front/AfficherEventDetails.html.twig',array('stands'=>$pagination,'evenements' => $evenement));
    }

    public function AjouterStandAction($id,Request $request){
        $stand = new Stand();

        $em = $this->getDoctrine()->getManager();
        $user=$this->getUser();

        $em = $this->getDoctrine()->getManager();
        $evenementaa = $em->getRepository(Evenement::class)->find($id);

        $reservationstand = $em->getRepository(Stand::class)->findBy(['user'=>$user,'idevenement'=>$evenementaa]);
        if($reservationstand!=null){
            $this->addFlash("alert", "Vous avier dejat reserver un stand");
            return $this->redirectToRoute('afficherEvenemntDetail',['id'=>$id]);
        }
        $evenement = $em->getRepository(Evenement::class)->findAll();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $evenement, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            2 /*limit per page*/
        );
        $evenemente = $em->getRepository(Evenement::class)->find($id);
        if($request->isMethod('POST')){

            $stand->setNumerostand($request->get('Numerostand'));
            $stand->setIdevenement($evenemente);
            $stand->setuser($user);
            $stand->setNomparticipant($user->getUsername());
            $stand->setNomevenement($evenemente->getNomevenement()); //TRES IMPORTANT!!!!!!!!!!
            $stand->setPrenomparticipant($user->getUsername());

            $stand->setImage($request->get('image'));



            $file=$request->files->get('image');
            $filName=md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('images_directory'),
                $filName);

            $stand->setImage($filName);



            $em = $this->getDoctrine()->getManager();






            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 25, 'tls')
                ->setUsername('firas.hamdi@esprit.tn')
                ->setPassword('boutmira565')
                ->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false)));
            $mailer = new \Swift_Mailer($transport);

            $message=(new \Swift_Message('traitement ...'))
                ->setFrom('firas.hamdi@esprit.tn')
                ->setTo('firas.hamdi@esprit.tn')
                ->setContentType('text/html')
                ->setBody($this->renderView(

                    '@Evenement/Front/emailajoutStand.html.twig',
                    ['stand'=>$stand]
                ),
                    'text/html'
                )
            ;

            $mailer->send($message);













            $em->persist($stand);
            $em->flush();
            return $this->redirectToRoute('afficherEvenemnt');
        }
        return $this->render('@Evenement/Front/formulaire.html.twig',array('users'=>$user,'evenements'=>$evenemente));

    }

    public function AfficherReservationAction(Request $request){
        $user=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $reservations = $em->getRepository(Reservation::class)->findBy(['user'=>$user]);

        return $this->render('@Evenement/Front/afficherReservation.html.twig',['reservations'=>$reservations]);
    }

    public function AfficherMesStandAction(Request $request){
        $user=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $reservations = $em->getRepository(Reservation::class)->findBy(['user'=>$user]);

        return $this->render('@Evenement/Front/afficherReservation.html.twig',['reservations'=>$reservations]);
    }


    public function AjouterReservationAction($id,Request $request){

        $user=$this->getUser();

        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository(Evenement::class)->find($id);


        $reservation = $em->getRepository(Reservation::class)->findBy(['user'=>$user,'event'=>$evenement]);
        if($reservation!=null){
            $this->addFlash("alert", "Vous avier dejat reserver");
            return $this->redirectToRoute('afficherEvenemntDetail',['id'=>$id]);
        }
        $reservation = new Reservation();
        if($request->isMethod('POST')){

            $reservation->setUser($user);
            $reservation->setEvent($evenement);
            $reservation->setDateReservation(new \DateTime('now'));
            $reservation->setNbPersonnes($request->get('nbPersonne'));


            $em = $this->getDoctrine()->getManager();
            $em->getRepository("EvenementBundle:Evenement")->decEvenement($request->get('nbPersonne'),$evenement->getIdevenement());
            $em->persist($reservation);
            $em->flush();

            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 25, 'tls')
                ->setUsername('firas.hamdi@esprit.tn')
                ->setPassword('boutmira565')
                ->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false)));
            $mailer = new \Swift_Mailer($transport);

            $message=(new \Swift_Message('Reservation '))
                ->setFrom('firas.hamdi@esprit.tn')
                ->setTo('firas.hamdi@esprit.tn')
                ->setContentType('text/html')
                ->setBody($this->renderView(

                    '@Evenement/Front/emailajoutReservation.html.twig',
                    ['reservations'=>$reservation,'evenements'=>$evenement,'users'=>$user]
                ),
                    'text/html'
                )
            ;
            $html = $this->renderView('@Evenement/Front/content.html.twig', ['reservations'=>$reservation,'evenements'=>$evenement,'users'=>$user]);
            $filename = 'Reservation.pdf';
            $pdf = $this->get("knp_snappy.pdf")->getOutputFromHtml($html);
            $attachement = \Swift_Attachment::newInstance($pdf, $filename, 'application/pdf' );
            $message->attach($attachement);


            $mailer->send($message);


            return $this->redirectToRoute('afficherEvenemnt');
        }
        return $this->render("@Evenement/Front/formulaireReservation.html.twig",array('users'=>$user,'evenements'=>$evenement));

    }



    /************************************************************************************************************/
    public function RechercheParNomEvenementAction(Request $request)
    {

        $nom =  $request->get("type");
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository("EvenementBundle:Evenement")->findEvenementParNom($nom);




        return $this->render("@Evenement/Front/afficherEvenementRecherche.html.twig",array('evenements'=>$modele));

    }


    public function CalendarAction(){
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository('EvenementBundle:Evenement')->FindAcceptedEv();
        return $this->render('@Evenement/Front/calendrierevent.html.twig', array(
            'evenements'=>$events
        ));
    }

    public  function LoadEvCalendarAction(){
        $em = $this->getDoctrine()->getManager();
        $listEv = $em->getRepository('EvenementBundle:Evenement')->FindAcceptedEv();
        $listEvJson = array();
        foreach ($listEv as $event)
            $listEvJson[] = array(
                'title' => $event->getNomevenement(),
                'start' => "" . ($event->getDatedebut()->format('Y-m-d')) . "",
                'end' => "" . ($event->getDatedebut()->format('Y-m-d')) . "",
                'groupId' => "" . ($event->getIdevenement()) . ""
            );
        return new JsonResponse(array('events' => $listEvJson));
    }

    public function EvenementDateTypeAction(Request $request)
    {
        $type = $request->get("type");
        $em = $this->getDoctrine()->getManager();


        if($type=='Aujourdhui')
        {
            $today_startdatetime = new\DateTime('today');
            $evenement = $em->getRepository(Evenement::class)->evenementaujourdhui($today_startdatetime);

        }
        else if($type=='Demain')
        {
            $tomorrow_startdatetime = new\DateTime('tomorrow');
            $evenement = $em->getRepository(Evenement::class)->evenementdemain($tomorrow_startdatetime);

        }
        else if($type=='Cettesemaine')
        {
            $today_startdatetime = new\DateTime('today');
            $thisweek_startdatetime = new\DateTime('today + 7day');
            $evenement = $em->getRepository(Evenement::class)->evenementcettesemaine($today_startdatetime,$thisweek_startdatetime);

        }
        else if($type=='Cemois')
        {
            $today_startdatetime = new\DateTime('today');
            $thismonth_startdatetime = new\DateTime('today + 30day');
            $evenement = $em->getRepository(Evenement::class)->evenementcemois($today_startdatetime,$thismonth_startdatetime);

        }
        else if ($type=='tout')

            {
                $em = $this->getDoctrine()->getManager();
                $evenement = $em->getRepository(Evenement::class)->findAll();
            }





        return $this->render('@Evenement/Front/afficherEvenementRecherche.html.twig', ['evenements' => $evenement]);
    }

}
