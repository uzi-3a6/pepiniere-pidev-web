<?php

namespace EvenementBundle\Form;

use EvenementBundle\Entity\Evenement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModeleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('idEvenement')->add('nomEvenement')->add('adresse')->add('datedebut')->add('datefin')->add('etat')->add('nbplace')->add('nbstand')->add('prix')->add('save',SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /*$resolver->setDefaults(array(
            'data_class' => 'EvenementBundle\Entity\Evenement'
        ));*/
        $resolver->setDefaults([
            'data_class' => Evenement::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
       // return 'esprit_parcbundle_modele';
    }


}
